# Dockerized Fork

## Git

Clone with:  
```bash
git clone --recurse-submodules <repo>
```

Or, if you already cloned, do:  
```bash
git submodule update --init
```

To update submodule, run:
```bash
git submodule update --remote
```

## Docker

Build: 
```
docker build -t fork .   
```

