FROM ubuntu:jammy AS build

RUN apt-get update && apt-get install -y \
    sudo \
    lsb-release \
    wget \
    software-properties-common \
    gnupg \
    binutils-dev \
    cmake \
    git 

ENV CLANG_VERSION 15

RUN wget https://apt.llvm.org/llvm.sh \
    && chmod +x llvm.sh \
    && sudo ./llvm.sh ${CLANG_VERSION}

RUN sudo update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++-${CLANG_VERSION} 999

WORKDIR /fork
COPY fork/ ./

WORKDIR /fork/build

RUN cmake -DCMAKE_BUILD_TYPE=Release .. && \
    cmake --build . --parallel $(nproc)

CMD [ "/bin/bash" ]
